# Simple Remote Control for Brick Build Trains

This is a simple project to control a brick built train from a webbrowser using

- ESP8266 module (e.g. Wemos D1 mini)
- Motor Driver Breakout (TB6612FNG e.g. from SparkFun)
- Websockets
- Update firmware and filesystem over the air (OTA) using ElegantOTA
- PlatformIO (as the programming environment: [PlatformIO](https://platformio.org)
- JavaScript based HTTP server for testing the page locally (see ws-test folder)

## Setup

Start with generating a `Network.h`  with your local settings based on the `Network_template.h` inside the [include folder](./include/Network_template.h).  
Your might need to adjust the settings inside the `motorConfig.h` located in the [include folder](./include/motorConfig.h) to your wiring as well.

## Building

This project is build with [PlatformIO](https://platformio.org) and may be build using `pio run` or if you are using e.g. VS Code `CTRL + ALT + B` (On Linux).  
For more information about PlatformIO see it's fine [Documentation](https://docs.platformio.org/en/latest/core/).  
Also do not forget to adopt the `platformio.ini` to your board used [PlatformIO Doc](https://docs.platformio.org/en/latest/projectconf/index.html).  
The first installation of the firmware needs to be done via a wired connection `CTRL + ALT + U`. Subsequent uploads can be done via EleganOTA at the address `http://<esp-ip>/update`.  
The remote's control webpage needs to be build as file system image (LittleFS) and uploaded after every change seperately. This can be done either via a wired connection or via the OTA GUI from the first time on. This very fine [LittleFS Tutorial](https://randomnerdtutorials.com/esp8266-nodemcu-vs-code-platformio-littlefs/) may help.  
The sources are located inside the `data` folder.

### Local Development

See [local webdev instructions](./ws-test/Readme.md).

### Hardware

The hardware setup, parts used and wiring is documented under [Hardware](./documentation/hardware.md).

### License

The project is released under the MIT license (as this seems to be the most common license amongst IoT projects)

This is a BrickInMotion project brought to you by BrickMotion.

![Motorized Locomotive](./documentation/motorized_locomotive.jpg)

![Motorized Steam Train](./documentation/pictures/stream_train.jpg)

![Remote Control ScreenShot](./documentation/remote_control.png)
