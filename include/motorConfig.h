#ifndef MOTOR_CONFIG_H
#define MOTOR_CONFIG_H

#define MOTOR_SPEED_COUNT_A 8
short unsigned int motorSpeedsA[] = {200, 300, 400, 500, 600, 700, 800, 900};

// Pins for all motor shield inputs,
// Motor A
#define AIN1 D7
#define AIN2 D6
// ! use a pin capable of PWM
#define PWMA D5

// Motor B
// #define BIN1 7
// #define BIN2 8
// ! use a pin capable of PWM
// #define PWMB 6

#define STBY D8

#endif // !MOTOR_CONFIG_H
