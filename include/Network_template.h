/**
 * copy this file to Network.h
 * replace placeholders with your local credentials below
 * and do not check your credentials into git!
 */

const char*  WIFI_SSID = "ENTER_WIFI_SSID_HERE";
const char*  WIFI_PASSWORD = "ENTER_WIFI_PASSWORD_HERE";
