/*
Released under the MIT license
*/
var wsUri = `ws://${window.location.host}/ws`
var websocket
var speedDisplay = document.getElementById('speed')
var batteryDisplay = document.getElementById('battery')

window.addEventListener('load', onLoad)

function onLoad(event) {
  initWebSocket()
  initEventListerner()
  document.getElementById("location").innerHTML =
    "The page is here:<br> ->" + window.location.host + " location.hostname: " + window.location.hostname
}

function initWebSocket() {
  console.log('Trying to open a WebSocket connection...')
  websocket = new WebSocket(wsUri)
  websocket.onopen = onOpen
  websocket.onclose = onClose
  websocket.onmessage = onMessage
  websocket.onerror = onError
}

function onOpen(event) {
  console.log('Connection opened')
  websocket.send("states")
}

function onClose(event) {
  console.log('Connection closed')
  setTimeout(initWebSocket, 2000)
}

function onError(error) {
  console.log('WebSocket Error ', error)
}

function onMessage(event) {
  const message = event.data
  console.log('Server: ', message)
  if (typeof(message) === 'string') {
    const details = message.split(':')
    switch (String(details[0])) {
      case "#Speed":
        speedDisplay.innerHTML = details[1]
        break
      case "#Battery":
        batteryDisplay.innerHTML = details[1]
        setBatteryLevelIndicator(details[1])
        break
      default:
        console.log('Unhandeled message', message)
    }
  }
}

function setBatteryLevelIndicator(batteryLevelMessage) {
  const parsedLevel = parseFloat(batteryLevelMessage.split(' ')[0])
  const battteryLevel = Math.round(parsedLevel * 100) ;
  var levelStyle = ""
  if (battteryLevel > 750) {
    levelStyle = "bLevel_good"
  } else if (battteryLevel > 500) {
    levelStyle = "bLevel_medium"
  } else if (battteryLevel > 460) {
    levelStyle = "bLevel_low"
  } else {
    levelStyle = "bLevel_poor"
  }
  var currentLevelStyle = ""
  for (var cl of batteryDisplay.classList.entries()) {
    if (String(cl[1]).startsWith("bLevel_")) {
      currentLevelStyle = cl[1]
      break
    }
  }
  if (currentLevelStyle != levelStyle) {
    console.log("Set new battery level style to: " + levelStyle)
    if (String(currentLevelStyle) != "") {
      batteryDisplay.classList.remove(currentLevelStyle)
    }
    batteryDisplay.classList.add(levelStyle)
  }
}

function sendData(id) {
  websocket.send('#' + id)
}


function initEventListerner() {
  var up_btn = document.getElementById('up_btn')
  var stop_btn = document.getElementById('stop_btn')
  var down_btn = document.getElementById('down_btn')

  up_btn.addEventListener('click', function () {
    sendData('UP')
  }, false)
  down_btn.addEventListener('click', function () {
    sendData('DOWN')
  }, false)
  stop_btn.addEventListener('click', function () {
    sendData('STOP')
  }, false)
}
