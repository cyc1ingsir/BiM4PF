class Websocket {

  constructor(parent) {
    this.speed = 0;
  }

  middleware(config, lws) {
    const WebSocket = require('ws')
    const wss = new WebSocket.Server({
      server: lws.server
    })

    wss.on('connection', socket => {
      socket.on('message', message => {
        console.log(`Received: ${message}`)
        this.handleMessage(socket, message)
      })
    })
  }

  handleMessage(socket, message) {
    switch (message) {
      case "#STOP":
        this.speed = 0
        break
      case "#DOWN":
        this.speed -= 100
        break
      case "#UP":
        this.speed += 100
        break
      default:
        console.log('Received unknown command!')
    }
    socket.send('#Speed:'+this.speed)
    if (this.speed < 100) {
      socket.send('#Battery:8.24 (1013)')
    } else if (this.speed < 200) {
      socket.send('#Battery:5.01 (759)')
    } else if (this.speed < 300) {
      socket.send('#Battery:4.70 (688)')
    } else {
      socket.send('#Battery:4.40 (596)')
    }
  }
}

module.exports = Websocket
