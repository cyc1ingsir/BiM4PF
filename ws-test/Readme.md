# Test the remote control's web page locally

## Setup

You may start a simple web-server (including a websocket) for testing the remote control page with the command `yarn dev` from inside a terminal.  
The page will be available on your [devlopers machine](http://localhost:3000).

## Additional Documentation

[Static Content served locally](https://github.com/lwsjs/local-web-server/wiki/How-to-serve-static-files)

[Websocket Plugin](https://github.com/lwsjs/local-web-server/wiki/How-to-create-a-Websocket-server)
