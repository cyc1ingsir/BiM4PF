/* *****************************************************************************
 * This is a library for the SparkFun TB6612 breakout board ROB-9457
 * It will probably work with any other
 * TB6612FNG H-Bridge Motor Driver board
 *
 **************************************************************************** */
#include "TB6612.h"

TB6612FngMotor::TB6612FngMotor(uint8 in1Pin, uint8 in2Pin, uint8 pwmPin, uint8 stbyPin,
                uint8 predefinedMotorSpeedCount, uint16_t* predefinedMotorSpeeds)
{
  m_In1Pin = in1Pin;
  m_In2Pin = in2Pin;
  m_PWMPin = pwmPin;
  m_standbyPin = stbyPin;
  m_motorSpeedCount = predefinedMotorSpeedCount;
  m_motorSpeeds = predefinedMotorSpeeds;

  pinMode(m_In1Pin, OUTPUT);
  pinMode(m_In2Pin, OUTPUT);
  pinMode(m_PWMPin, OUTPUT);
  pinMode(m_standbyPin, OUTPUT);
  brake();
  analogWriteFreq(1000);
  analogWriteRange(1023);
}

int16_t TB6612FngMotor::increaseSpeed()
{
   return adjustMotorSpeed(FORWARD);
}

int16_t TB6612FngMotor::decreaseSpeed()
{
   return adjustMotorSpeed(REVERSE);
}

int16_t TB6612FngMotor::adjustMotorSpeed(Motor_Direction step)
{
  if (m_motorSpeedCount == 0 || m_motorSpeeds == nullptr) {
     return 0;
  }
  switch (m_direction)
  {
  case IDLE:
     // setting new direction to either FORWARD or REVERSE
     m_direction = step;
     // and choose first speed
     m_motorSpeedIdx = 1;
     break;
  case FORWARD:
     m_motorSpeedIdx += step;
     break;
  case REVERSE:
     m_motorSpeedIdx -= step;
     break;
  default:
     Serial.printf("ERROR: This should not have happend!\n");
     // undefined state better brake and return
     brake();
     return 0;
     break;
  }
  if (m_motorSpeedIdx == 0) {
     m_direction = IDLE;
     brake();
     return 0;
  }
  int16_t newSpeed = 0;
  if (m_motorSpeedIdx <= m_motorSpeedCount) {
     newSpeed = m_motorSpeeds[m_motorSpeedIdx - 1] * m_direction;
     drive(newSpeed);
  } else {
     m_motorSpeedIdx--;
     newSpeed = m_motorSpeeds[m_motorSpeedIdx - 1] * m_direction;
  }

  Serial.printf("Index: %u, Direction: %s, Speed: %i\n",
      m_motorSpeedIdx, (m_direction == FORWARD)? "FORWARD" : "REVERSE", newSpeed);

  return newSpeed;

}

void TB6612FngMotor::drive(int16_t speed)
{
  Serial.printf("New speed: %i\n", speed);
  if (speed == 0) {
    brake();
  }
  digitalWrite(m_standbyPin, HIGH);
  if (speed>=0) fwd(speed);
  else rev(-speed);
}

void TB6612FngMotor::fwd(uint16 speed)
{
  digitalWrite(m_In1Pin, HIGH);
  digitalWrite(m_In2Pin, LOW);
  analogWrite(m_PWMPin, speed);

}

void TB6612FngMotor::rev(uint16_t speed)
{
  digitalWrite(m_In1Pin, LOW);
  digitalWrite(m_In2Pin, HIGH);
  analogWrite(m_PWMPin, speed);
}

void TB6612FngMotor::brake()
{
  analogWrite(m_PWMPin,0);
  m_motorSpeedIdx = 0;
  m_direction = IDLE;
  standby();
  Serial.printf("BRAKE \n");
}

void TB6612FngMotor::standby()
{
  digitalWrite(m_standbyPin, LOW);
  digitalWrite(m_In1Pin, LOW);
  digitalWrite(m_In2Pin, LOW);
}
