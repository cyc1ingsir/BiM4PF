/* *****************************************************************************
 * This is a library for the SparkFun TB6612 breakout board ROB-9457
 * It will probably work with any other
 * TB6612FNG H-Bridge Motor Driver board
 * SparkFuns original library may be found at
 * https://github.com/sparkfun/SparkFun_TB6612FNG_Arduino_Library
 *
 **************************************************************************** */

#ifndef TB6612_H
#define TB6612_H

#include <Arduino.h>

typedef enum
{
  IDLE = 0,
  FORWARD = 1,
  REVERSE = -1
} Motor_Direction;

class TB6612FngMotor
{
  public:
    // Constructor. Mainly sets up pins.
    TB6612FngMotor(uint8 in1Pin, uint8 in2Pin, uint8 pwmPin, uint8 stbyPin,
                uint8 predefinedMotorSpeedCount = 0, uint16_t* predefinedMotorSpeeds = nullptr);

    // Drive in direction given by sign, at passed speed
    void drive(int16_t speed);

    int16_t increaseSpeed();

    int16_t decreaseSpeed();

    //Stops motor by setting both input pins high
    void brake();

    //set the chip to standby mode.  The drive function takes it out of standby
    //(forward, back, left, and right all call drive)
    void standby();

  private:
    //variables for the 2 inputs, PWM input, and the Standby pin
    uint8 m_In1Pin, m_In2Pin, m_PWMPin, m_standbyPin;

    uint8 m_motorSpeedCount;
    uint16_t* m_motorSpeeds;

    uint8 m_motorSpeedIdx = 0;
    Motor_Direction m_direction = IDLE;

    int16_t adjustMotorSpeed(Motor_Direction step);
    //private functions that spin the motor CC and CCW
    void fwd(uint16 speed);
    void rev(uint16_t speed);

};

#endif //TB6612_H
