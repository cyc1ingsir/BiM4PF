#ifndef WEBSERVICE_H
#define WEBSERVICE_H

#include <Arduino.h>

#if defined(ESP8266)
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#endif
#include <ESPAsyncWebServer.h>

typedef std::function<int(const char *message)> WebServiceMessageHandlerFunction;

class WebService {

  public:
    WebService(uint16_t port = 80);

    void begin();

    void loop();

    void onWsTextMessage(WebServiceMessageHandlerFunction fn) { m_onWsMessage = fn; }

    void sendBatteryLevel(const char* level);

  private:
    void onRequest(AsyncWebServerRequest *request);

    void onBody(AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total);

    void onEvent(AsyncWebSocket * server, AsyncWebSocketClient * client, AwsEventType type, void * arg, uint8_t *data, size_t len);


    AsyncWebServer m_server;
    AsyncWebSocket m_ws; // access at ws://[esp ip]/ws

    WebServiceMessageHandlerFunction m_onWsMessage;

};

#endif // WEBSERVICE_H
