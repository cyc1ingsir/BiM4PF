#include "webservice.h"
#include <functional> // for function pointers
#include <FS.h>
#include <LittleFS.h>
#include <AsyncElegantOTA.h>

WebService::WebService(uint16_t port) :
  m_server(AsyncWebServer(port))
  , m_ws(AsyncWebSocket("/ws")) // access at ws://[esp ip]/ws
{
  if (!LittleFS.begin()) {
    Serial.printf("ERROR on mounting LittleFS");
    return;
  }
  Serial.println("LittleFS mounted successfully");
  m_onWsMessage = nullptr;
}

void WebService::begin() {

#ifdef DEBUG_FS
  if(LittleFS.exists("/")) {
    Serial.printf("Listing content of / on LittleFS!");
    Dir dir = LittleFS.openDir("/");
    while(dir.next()) {
      Serial.printf("=> %s \n", dir.fileName().c_str());
    }
  } else {
    Serial.printf("Path / not found on LittleFS!");
  }
#endif

  // attach AsyncWebSocket
  m_ws.onEvent(std::bind(&WebService::onEvent, this, std::placeholders::_1, std::placeholders::_2,
   std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6));
  m_server.addHandler(&m_ws);

  // respond to GET requests on URL /heap
  m_server.on("/heap", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(200, "text/plain", String(ESP.getFreeHeap()));
  });

  m_server.serveStatic("/", LittleFS, "/").setDefaultFile("index.html");

  // Catch-All Handlers
  // Any request that can not find a Handler that canHandle it
  // ends in the callbacks below.
  m_server.onNotFound(std::bind(&WebService::onRequest, this, std::placeholders::_1));
  m_server.onRequestBody(std::bind(&WebService::onBody, this, std::placeholders::_1, std::placeholders::_2,
   std::placeholders::_3, std::placeholders::_4, std::placeholders::_5));

  // Start ElegantOTA
  AsyncElegantOTA.begin(&m_server);

  m_server.begin();

}

void WebService::loop() {
  AsyncElegantOTA.loop();
  m_ws.cleanupClients();
}

void WebService::onRequest(AsyncWebServerRequest *request) {
  // Handle Unknown Request
  Serial.printf("Request received for %s \n", request->url().c_str());
  request->send(404);
}

void WebService::onBody(AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total) {
  // Handle body
  Serial.printf("Received body with length: %u \n", len);
}


void WebService::onEvent(AsyncWebSocket * server, AsyncWebSocketClient * client, AwsEventType type, void * arg, uint8_t *data, size_t len) {
  if(type == WS_EVT_CONNECT) {
    // client connected
    Serial.printf("ws[%s][%u] connect\n", server->url(), client->id());
    client->printf("Hello Client %u :)", client->id());
    client->ping();
  } else if(type == WS_EVT_DISCONNECT) {
    // client disconnected
    Serial.printf("ws[%s] disconnect: %u\n", server->url(), client->id());
    // it might be wise to stop the train if the client disconnects
    const char* stopMessage = "#STOP";
    m_onWsMessage(stopMessage);
  } else if(type == WS_EVT_ERROR) {
    // error was received from the other end
    Serial.printf("ws[%s][%u] error(%u): %s\n", server->url(), client->id(), *((uint16_t*)arg), (char*)data);
  } else if(type == WS_EVT_PONG) {
    // pong message was received (in response to a ping request maybe)
    Serial.printf("ws[%s][%u] pong[%u]: %s\n", server->url(), client->id(), len, (len)?(char*)data:"");
  } else if(type == WS_EVT_DATA) {
    // data packet
    AwsFrameInfo * info = (AwsFrameInfo*)arg;
    if(info->final && info->index == 0 && info->len == len) {
      // the whole message is in a single frame and we got all of it's data
      Serial.printf("ws[%s][%u] %s-message[%llu]: ", server->url(), client->id(), (info->opcode == WS_TEXT)?"text":"binary", info->len);
      if(info->opcode == WS_TEXT) {
        data[len] = 0;
        Serial.printf("%s\n", (char*)data);
        if (m_onWsMessage != nullptr) {
          int speed = m_onWsMessage((char*) data);
          // report current speed to the client
          client->printf("#Speed:%i", speed);
        } else {
          Serial.printf("WARN: No message handler was added\n");
        }
      } else {
        for(size_t i=0; i < info->len; i++) {
          Serial.printf("%02x ", data[i]);
        }
        Serial.printf("\n");
      }
    } else {
      // message is comprised of multiple frames or the frame is split into multiple packets
      // will not be handled - not needed for this project
      Serial.printf("message with multiple frames not handled\n");
    }
  }
}

void WebService::sendBatteryLevel(const char* level) {

  char* buffer = new char[35];
  size_t len = snprintf(buffer, 35, "#Battery:%s", level);

  m_ws.textAll(buffer, len);
}
