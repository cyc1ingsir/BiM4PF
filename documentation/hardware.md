# Hardware

## Used Parts

A list of the hardware used.  
This is to be seen as an example only. The links are *no* affiliate links. The examples are provided to help your choosing your own parts.

- [Wemos D1 Mini ESP8266](https://www.berrybase.de/dev.-boards/esp8266-esp32-d1-mini/boards/d1-mini-esp8266-entwicklungsboard)
- [Mini Step-Down Converter einstellbar 4,75-23V -> 1-17V / 3A](https://www.berrybase.de/strom/netzteile/netzteilmodule/mini-step-down-converter-einstellbar-4-75-23v-1-17v/3a?c=97)
- [Subminiatur Switch, 2 Pin, ON-OFF](https://www.berrybase.de/bauelemente/schalter-taster/kippschalter/subminiatur-kippschalter-2-pin-ein-aus)
- [2x 18650 Li-Ion Akku with integrated PCB - 2600mAh](https://www.berrybase.de/digibuddy-18650-li-ion-akku-mit-integriertem-pcb-2600mah-2-st-252-ck-in-kunststoffbox)
- [Akku Box for two 18650](https://www.berrybase.de/strom/batterien-akkus/installation-zubehoer/batterie-clips-halter/batteriehalter-fuer-sonstige-zellen/batteriehalter-f-252-r-2x-18650-mit-anschlusskabel)
- [SparkFun Motor Treiber, Dual TB6612FNG, mit Headern](https://www.berrybase.de/neu/sparkfun-motor-treiber-dual-tb6612fng-mit-headern?c=2433)
- A couple of LEDs (currently yellow 3mm, 2.1V, 11mA and rew 3mm, 1.9V, 9mA)
- A couple of resistors (100 Ohm for the yellow LED; 150 Ohm for the red LED)
- [JST PH 2.0 Plug](https://www.berrybase.de/bauelemente/steckverbinder/rastersteckverbinder-2-00mm/kabel-lipo-kompatibel-mit-2-pin-jst-ph-2.0mm-steckverbinder-awg26-20cm)
- [JST PH 2.0 Socket](https://www.berrybase.de/bauelemente/steckverbinder/rastersteckverbinder-2-00mm/ph2.0-buchse-printmontage-gewinkelt-2-pin)
- [Train Motor](https://www.bluebrixx.com/en/assortments/401172/Railway-engine-BlueBrixx-Special)
- [Brick Locomotive BR92](https://www.bluebrixx.com/en/bluebrixxspecials/102531/Steam-locomotive-BR-92-BlueBrixx-Special)
- [Motorisable Freight Wagon](https://www.bluebrixx.com/en/trains/103291/Motorisable-freight-wagon-BlueBrixx-Special) => This was modified as the original was considered to long
- Tender -> own designs made from bricks

## Schematics

For howto wire together see this [Fritzing file](./d1-mini-train-motor.fzz).
![Breadboard Wiring Diagram](./wiring.png)

![control electronic inside wagon](./pictures/control_board_inside.jpg)

![connected D1 mini board](./pictures/d1_board.jpg)

![motor driver board](./pictures/motor_driver_board.jpg)

![freight wagon](./pictures/motor_freight_wagon.jpg)
