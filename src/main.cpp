#include <Arduino.h>

#include <Network.h>
#include <webservice.h>

// library for the TB6612 motor driver
#include <TB6612.h>
#include <motorConfig.h>


TB6612FngMotor motor1 = TB6612FngMotor(AIN1, AIN2, PWMA, STBY, MOTOR_SPEED_COUNT_A,   motorSpeedsA);
// TB6612FngMotor motor2 = Motor(BIN1, BIN2, PWMB, STBY);

WebService webservice(80);

boolean currentlyMoving = false;

/**
 * @brief Handle message received via websocket
 * This method will adjust the motor speed according
 * to the message received currently only
 *
 * @param message
 * @return int
 */
int handleWsTextMessage(const char* message) {
  Serial.printf("Going to handle WS text message - %s - \n", message);

  if (strncmp(message, "#STOP", 5) == 0) {
    Serial.printf("Handling #STOP\n");
    motor1.brake();
    currentlyMoving = false;
    digitalWrite(D1, LOW);
    digitalWrite(D2, LOW);
    return 0;
  }

  int16_t newSpeed = 0;
  if (strncmp(message, "#DOWN", 5) == 0) {
    Serial.printf("Handling #DOWN\n");
    newSpeed = motor1.decreaseSpeed();
  }
  if (strncmp(message, "#UP", 3) == 0) {
    Serial.printf("Handling #UP\n");
    newSpeed = motor1.increaseSpeed();
  }

  if (newSpeed == 0) {
      digitalWrite(D1, LOW);
      digitalWrite(D2, LOW);
      currentlyMoving = false;
  } else if (newSpeed > 0) {
      digitalWrite(D1, HIGH);
      digitalWrite(D2, LOW);
      currentlyMoving = true;
  } else {
      digitalWrite(D1, LOW);
      digitalWrite(D2, HIGH);
      currentlyMoving = true;
  }
  return newSpeed;

  // Serial.printf("Unknown message: %s\n", message);
}

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(D1, OUTPUT);
  pinMode(D2, OUTPUT);
  digitalWrite(D1,LOW);
  digitalWrite(D2, HIGH); // indicate startup with connected LED
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
   // Set Wifi persistent to false,
  // otherwise on every WiFi.begin the
  // ssid and password will be written to
  // the same area in the flash which will
  // destroy the device in the long run
  // See: https://github.com/esp8266/Arduino/issues/1054
  WiFi.persistent(false);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.printf("WiFi Failed!\n");
    return;
  }
  Serial.printf("Listening on: %s\n", WiFi.localIP().toString().c_str());
  webservice.onWsTextMessage(handleWsTextMessage);

  webservice.begin();
  digitalWrite(D2, LOW);
  digitalWrite(LED_BUILTIN, HIGH);
}

/**
 * Tick will return true if
 * at least one second has passed
 * since this method was called last
 *
 */
uint32_t lastMillis = 0;
boolean tick() {
  uint32_t now = millis();
  if (now >= lastMillis) {
    if ((now - lastMillis) >= 1000) {
      lastMillis = now;
      return true;
    }
  } else {
    // overflow of millis has happened
    // resetting last millis only
    Serial.printf("Overflow of millis!\n");
    lastMillis = now;
  }
  return false;
}

/**
 * Try to reconnect to the WiFi
 * call this function if connection ot WiFi
 * was lost.
 *
 */
boolean currentlyReconnecting = false;
boolean reconnectWasNeeded = false;
void tryReconnect() {
  reconnectWasNeeded = true;
  Serial.printf("No longer connected to WIFI - trying to reconnect ...\n");
  WiFi.persistent(false);
  for (uint8_t i = 0; i<=5; i++) {
    WiFi.reconnect();
    if(WiFi.waitForConnectResult() != WL_CONNECTED) {
      if (i == 5) {
        // Emergency brake after 5 seconds without WiFi connection
        motor1.brake();
        currentlyMoving = false;
        Serial.printf("!!Unable to reconnect to WIFI!! Emergency Brake!!\n");
      }
      digitalWrite(LED_BUILTIN, HIGH);
      digitalWrite(D1, LOW);
      digitalWrite(D2, HIGH);
      delay(1000);
      digitalWrite(LED_BUILTIN, LOW);
      digitalWrite(D1, HIGH);
      digitalWrite(D2, LOW);
    } else {
      break;
    }
  }
}

/**
 * Toogle all connected LEDs
 * in a round robin kind of manner
 * e.g. to indicate power is on
 */
enum LedStates {
  LED_OFF = 0,
  LED_BUILTIN_ON = 1,
  LED_YELLOW_ON = 2,
  LED_RED_ON = 3,
  LED_END = 4
};
uint8_t currentLedState = LED_OFF;
void toggleLeds() {
  currentLedState++;
  if(currentLedState == LED_END) {
    currentLedState = LED_OFF;
  }
  digitalWrite(LED_BUILTIN, HIGH);
  digitalWrite(D1, LOW);
  digitalWrite(D2, LOW);
  switch (currentLedState)
  {
  case LED_BUILTIN_ON:
    digitalWrite(LED_BUILTIN, LOW);
    break;
  case LED_RED_ON:
    digitalWrite(D2, HIGH);
    break;
  case LED_YELLOW_ON:
    digitalWrite(D1, HIGH);
    break;
  default:
    break;
  }
}

#define BATTERY_LEVELS 4
int batteryLevels[BATTERY_LEVELS];
char batteryLevelBuffer[15];
void sendBatteryLevel() {
  // int rawLevel = analogRead(A0);
  int batteryLevelSum = 0;
  for (uint8_t i = 0; i < BATTERY_LEVELS; i++) {
    batteryLevelSum+= batteryLevels[i];
  }
  int rawLevel = batteryLevelSum / BATTERY_LEVELS;
  float level = 8.4f * (float)rawLevel / 1024.0f;
  Serial.printf(PSTR("Raw Battery Level: %.2f (%4u)\n"), level, rawLevel);
  snprintf(batteryLevelBuffer, 15, PSTR("%.2f (%u)"), level, (rawLevel));
  webservice.sendBatteryLevel(batteryLevelBuffer);
}

uint8_t batteryLevelsIdx = 0;
void readBatteryLevel() {
  batteryLevels[batteryLevelsIdx] = analogRead(A0);
  Serial.printf(PSTR("Analog Read : (%4i)\n"), batteryLevels[batteryLevelsIdx]);
  batteryLevelsIdx++;
  if (batteryLevelsIdx == BATTERY_LEVELS) {
    batteryLevelsIdx = 0;
  }
}

/**
 * Main Loop
 *
 */
uint8_t secondsCounter = 0;
void loop() {

  if (tick()) {
    secondsCounter++;
    if (secondsCounter % 5 == 0) {
      // check if WiFi is still alive roughly every 5 seconds
      if (!WiFi.isConnected()) {
        tryReconnect();
      }
    }
    if (!currentlyMoving) {
      if (reconnectWasNeeded) {
        // togle LEDs every second if reconnecting to WiFi was
        // needed at least once!
        toggleLeds();
      } else if (secondsCounter % 3 == 0) {
        toggleLeds();
      }
    }
    if (secondsCounter % 3 == 0) {
      readBatteryLevel();
    }
    if (secondsCounter % 12 == 0) {
      sendBatteryLevel();
    }
    if (secondsCounter >= 60) {
      secondsCounter = 0;
    }

    webservice.loop();
  }
}
